<?php

/**
 * Implements hook_schema().
 */
function horoscopes_schema() {
  $schema['horoscopes'] = array(
    'description' => 'The base table for horoscopes.',
    'fields' => array(
      'hid' => array(
        'description' => 'The primary identifier for a horoscope.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The {users}.uid that owns this horoscope; initially, this is the user that created it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether the horoscope is published (visible to non-administrators).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the node was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the node was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('hid'),
  );

  $schema['horoscopes_data'] = array(
    'description' => 'Table for horoscopes data.',
    'fields' => array(
      'hdid' => array(
        'description' => 'Data id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'attached_to' => array(
        'description' => 'To what horoscope holder this data entity is attached.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('hdid'),
  );

  return $schema;
}
