<?php
/**
 * @file
 * Admin ui for horoscopes.
 */

function horoscopes_manage_form($form, &$form_state) {
  return $form;
}

function horoscope_form($entity, $entity_type) {
  $form = $form_state = array();
  if (module_exists('metatag')) {
    variable_set('metatag_enable_horoscope', FALSE);
  }

  field_attach_form('horoscope', $entity, $form, $form_state);

  return $form;
}

function horoscope_form_submit($form, &$form_state) {
  $horoscope = entity_ui_form_submit_build_entity($form, $form_state);
  $horoscope->save();
  drupal_set_message(t('Horoscope has been saved.'));
  $form_state['redirect'] = 'admin/content/horoscopes';
}



function horoscope_create_set_form($form, $form_state_) {
  return horoscopes_survey_form($form, $form_state);
}
