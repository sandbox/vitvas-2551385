<?php


function horoscope_create_form_steps() {
  return array(
    'select_period',
    'select_year',
    'select_month',
    'select_week',
    'select_day',
    'data_for_sign_1',
    'data_for_sign_2',
    'data_for_sign_3',
    'data_for_sign_4',
    'data_for_sign_5',
    'data_for_sign_6',
    'data_for_sign_7',
    'data_for_sign_8',
    'data_for_sign_9',
    'data_for_sign_10',
    'data_for_sign_11',
    'data_for_sign_12',
    'save_sign_data',
  );
}

function horoscopes_get_step_name($step) {
  $steps = horoscope_create_form_steps();
  if (isset($steps[$step])) {
    return $steps[$step];
  }
}


function horoscopes_survey_form($form, &$form_state) {
  if (!isset($form_state['step'])) $form_state['step'] = 0;

  $step = &$form_state['step'];

  $form = array();
  $step_name = horoscopes_get_step_name($step);

  if (strpos($step_name, 'data_for_sign') !== FALSE) {
    $function = 'horoscopes_survey_data_for_sign';
  }
  else {
    $function = 'horoscopes_survey_' . $step_name;
  }

  if (function_exists($function . '_validate')) {
    $form['#validate'][] = $function . '_validate';
  }

  if (function_exists($function . '_submit')) {
    $form['#submit'][] = $function . '_submit';
  }

  $form['#submit'][] = 'horoscopes_survey_form_submit';

  $form = call_user_func_array($function, array($form, $form_state));

  $form['buttons'] = array(
    '#type' => 'container',
    '#arrtibutes' => array(),
  );
  $form['buttons'] += horoscopes_survey_form_buttons($step);

  return $form;
}

function horoscopes_survey_form_buttons($step) {
  if ($step > 0) {
    $form['previous'] = array(
      '#type' => 'submit',
      '#name' => 'previous',
      '#value' => t('Previous'),
    );
  }

  $form['next'] = array(
    '#type' => 'submit',
    '#name' => 'next',
    '#value' => t('Next'),
  );

  return $form;
}

function horoscopes_survey_is_step_valid($step, $storage) {
  $steps = horoscope_create_form_steps();
  switch ($steps[$step]) {
    case 'select_month':
      if ($storage['period'] == 'week' || $storage['period'] == 'year') {
        return FALSE;
      }
      break;

    case 'select_week':
      if ($storage['period'] !== 'week') {
        return FALSE;
      }
      break;

    case 'select_day':
      if ($storage['period'] !== 'day') {
        return FALSE;
      }
      break;
  }
  return TRUE;
}


function horoscopes_survey_form_submit($form, &$form_state) {
  $step = &$form_state['step'];
  $last_step = count(horoscope_create_form_steps()) - 1;

  switch ($form_state['triggering_element']['#name']) {
    case 'next':
      if ($step != $last_step) {
        $form_state['rebuild'] = TRUE;
        $step++;
        while (!horoscopes_survey_is_step_valid($step, $form_state['storage'])) {
          $step++;
        }
      }
      else {
        $form_state['redirect'] = 'admin/content/horoscopes';
      }
      break;

    case 'previous':
      $form_state['rebuild'] = TRUE;
      $step--;
      while (!horoscopes_survey_is_step_valid($step, $form_state['storage'])) {
        $step--;
      }
      break;
  }

}

function horoscopes_survey_select_period($form, $form_state) {
  $form['period'] = array(
    '#title' => t('Period'),
    '#type' => 'select',
    '#options' => array(
      'day' => t('Day'),
      'week' => t('Week'),
      'month' => t('Month'),
      'year' => t('Year'),
    ),
    '#default_value' => isset($form_state['storage']['period']) ? $form_state['storage']['period'] : 'day',
    '#required' => TRUE,
  );

  return $form;
}

function horoscopes_survey_select_period_submit($form, &$form_state) {
  $form_state['storage']['period'] = $form_state['values']['period'];
}


function horoscopes_survey_select_year($form, $form_state) {
  $current_year = intval(date('Y', time()));

  $form['year'] = array(
    '#title' => t('Year'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range($current_year - 10, $current_year + 10)),
    '#default_value' => isset($form_state['storage']['year']) ? $form_state['storage']['year'] : $current_year,
  );

  return $form;
}

function horoscopes_survey_select_year_submit($form, &$form_state) {
  $form_state['storage']['year'] = $form_state['values']['year'];
}

function horoscopes_survey_select_month($form, $form_state) {
  $current_month = intval(date('M', time()));

  $form['month'] = array(
    '#title' => t('Month'),
    '#type' => 'select',
    '#options' => array(
      '1' => t('January'),
      '2' => t('February'),
      '3' => t('March'),
      '4' => t('April'),
      '5' => t('May'),
      '6' => t('June'),
      '7' => t('July'),
      '8' => t('August'),
      '9' => t('September'),
      '10' => t('October'),
      '11' => t('November'),
      '12' => t('December'),
    ),
    '#required' => TRUE,
    '#default_value' => isset($form_state['storage']['month']) ? $form_state['storage']['month'] : $current_month,
  );

  return $form;
}

function horoscopes_survey_select_month_submit($form, &$form_state) {
  $form_state['storage']['month'] = $form_state['values']['month'];
}

function horoscopes_survey_select_week($form, $form_state) {
  $form['week'] = array(
    '#title' => t('Week number'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range(1, 52)),
    '#required' => TRUE,
    '#default_value' => isset($form_state['storage']['week']) ? $form_state['storage']['week'] : 1,
  );

  return $form;
}

function horoscopes_survey_select_week_submit($form, &$form_state) {
  $form_state['storage']['week'] = $form_state['values']['week'];
}

function horoscopes_survey_select_day($form, &$form_state) {
  $form['day'] = array(
    '#title' => t('Day number'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range(1, 31)),
    '#required' => TRUE,
    '#default_value' => isset($form_state['storage']['day']) ? $form_state['storage']['day'] : 1,
  );

  return $form;
}

function horoscopes_survey_select_day_submit($form, &$form_state) {
  $form_state['storage']['day'] = $form_state['values']['day'];
}

function horoscopes_survey_data_for_sign($form, &$form_state) {
  $step_name = horoscopes_get_step_name($form_state['step']);
  if (isset($form_state['storage'][$step_name])) {
    $entity = $form_state['storage'][$step_name];
  }
  else {
    $entity = entity_create('horoscope', array('type' => 'horoscope'));
  }

  $form['sign'] = array(
    '#type' => 'fieldset',
    '#title' => 'Horoscope sign name',
  );

  $form['sign'][0] = array();

  $info = entity_get_info('horoscope');
  if ($info['fieldable']) {
    $entity_form = & $form['sign'][0];
    $langcode = entity_language('horoscope', $entity);
    field_attach_form('horoscope', $entity, $entity_form, $form_state, $langcode);
  }
  return $form;
}

function horoscopes_survey_data_for_sign_submit($form, &$form_state) {
  $step_name = horoscopes_get_step_name($form_state['step']);
  $info = entity_get_info('horoscope');
  $entity_form = $form['sign'][0];
  list(, , $bundle) = entity_extract_ids('horoscope', $entity_form['#entity']);
  $entity = $entity_form['#entity'];
  $entity_values = drupal_array_get_nested_value($form_state['values'], $entity_form['#parents']);

  // Copy top-level form values that are not for fields to entity properties,
  // without changing existing entity properties that are not being edited by
  // this form. Copying field values must be done using field_attach_submit().
  $values_excluding_fields = $info['fieldable'] ? array_diff_key($entity_values, field_info_instances('horoscope', $bundle)) : $entity_values;
  foreach ($values_excluding_fields as $key => $value) {
    $entity->$key = $value;
  }

  if ($info['fieldable']) {
    field_attach_submit('horoscope', $entity, $entity_form, $form_state);
  }

  $form_state['storage'][$step_name] = $entity;

  // When form is being rebuilt it takes default values from input.
  foreach (array_keys(field_info_instances('horoscope', $bundle)) as $field_name) {
    unset($form_state['input'][$field_name]);
  }
}

function horoscopes_survey_save_sign_data($form, $form_state) {
  $form['message'] = array(
    '#markup' => t('This is the last step, all data will be saved after this step.'),
  );
  return $form;
}


function horoscopes_survey_save_sign_data_submit($form, $form_state) {
  if ($form_state['triggering_element']['#name'] == 'next') {
    foreach (horoscope_create_form_steps() as $step_name) {
      $storage = $form_state['storage'];
      if (isset($storage[$step_name]) && $storage[$step_name] instanceof Horoscope) {
        entity_save('horoscope', $form_state['storage'][$step_name]);
        drupal_set_message('created entity');
      }
    }
  }

}
