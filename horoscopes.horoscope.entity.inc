<?php
/**
 * @file
 * Horoscope entity classes.
 */

class HoroscopeController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );

    return parent::create($values);
  }
}

class HoroscopeUIController extends EntityDefaultUIController {

}

class Horoscope extends Entity {

  protected function defaultUri() {
    return array('path' => 'horoscope/' . $this->identifier());
  }

  protected function defaultLabel() {
    return '@TODO change label';
  }
}